"use strict";

// Асинхронність в JavaScript означає, що код може виконуватись паралельно, без очікування завершення попередніх операцій.
// Написати програму "Я тебе знайду по IP"

// Технічні вимоги:
// Створити просту HTML-сторінку з кнопкою Знайти по IP.
// Натиснувши кнопку - надіслати AJAX запит за адресою https://api.ipify.org/?format=json, отримати звідти IP адресу клієнта.
// Дізнавшись IP адресу, надіслати запит на сервіс https://ip-api.com/ та отримати інформацію про фізичну адресу.
// під кнопкою вивести на сторінку інформацію, отриману з останнього запиту – континент, країна, регіон, місто, район.
// Усі запити на сервер необхідно виконати за допомогою async await.

const URL = "https://api.ipify.org/?format=json";
const button = document.querySelector("button");

async function getIp(url) {
  const response = await fetch(url);
  const data = await response.json();
  return data;
}

async function getAdress(ip) {
  const response = await fetch(`http://ip-api.com/json/${ip}`);
  const data = await response.json();
  return data;
}

button.addEventListener("click", async () => {
  const ipAdress = await getIp(URL);
  const Adress = await getAdress(ipAdress.ip);
  let { countryCode, country, city, regionName, region } = Adress;
  const adress = document.createElement("p");
  adress.textContent = `${countryCode} - ${country} - ${regionName} (номер регіону ${region}) - ${city} `;

  button.after(adress);

  // .then((data) => getAdress(data.ip))
  // .then((data) => {
  //   const adress = document.createElement("p");
  //   let { countryCode, country, city, regionName, region } = data;
  //   adress.textContent = `${countryCode} - ${country} - ${regionName} (номер регіону ${region}) - ${city} `;

  //   button.after(adress);
  // });
});
